#ifndef ALG_MEDIANMAINTAINER_HPP
#define ALG_MEDIANMAINTAINER_HPP

#include <vector>
#include <queue>
#include <functional>

namespace alg
{

class medianMaintainer
{
public:
    void insert(unsigned n)
    {
        if (heapLow_.empty() && heapHigh_.empty()) heapLow_.push(n);
        else if (heapLow_.empty() && n <= heapHigh_.top()) heapLow_.push(n); 
        else if (heapLow_.empty() && n > heapHigh_.top()) heapHigh_.push(n); 
        else if (n <= heapLow_.top()) heapLow_.push(n);
        else heapHigh_.push(n);

        balanceHeaps();
    }

    unsigned getMedian()
    {
        if (heapHigh_.size() > heapLow_.size())
            return heapHigh_.top();
        return heapLow_.top();
    }

private:
    void balanceHeaps()
    {
        if (static_cast<int>(heapLow_.size() - heapHigh_.size()) > 1)
        {
            auto top = heapLow_.top();
            heapLow_.pop();
            heapHigh_.push(top);
        }
        else if(static_cast<int>(heapHigh_.size() - heapLow_.size()) > 1)
        {
            auto top = heapHigh_.top();
            heapHigh_.pop();
            heapLow_.push(top);
        }
    }

private:
    std::priority_queue<unsigned> heapLow_;
    std::priority_queue<unsigned, std::vector<unsigned>, std::greater<unsigned>> heapHigh_;
};

}  // namespace alg

#endif  // ALG_MEDIANMAINTAINER_HPP
