#include <fstream> 
#include <iostream>
#include <algorithm>
#include <iterator>
#include "medianMaintainer.hpp"

int main(int argc, char** argv)
{
    unsigned medianSum = 0;
    unsigned number;
    alg::medianMaintainer maintainer;
    std::ifstream inputFile(argv[1]);

    while (inputFile >> number)
    {
        maintainer.insert(number);
        medianSum += maintainer.getMedian();
    }

    std::cout << medianSum % 10000;
    return 0;
}
