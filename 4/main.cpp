#include <fstream> 
#include <iostream>
#include <functional> 
#include <algorithm>
#include <iterator>
#include "graph.hpp"

void printSizes(const std::vector<unsigned>& sizes)
{
    std::copy_n(sizes.begin(), 5, std::ostream_iterator<unsigned>(std::cout, ","));
}

int main(int argc, char** argv)
{
    std::ifstream inputFile(argv[1]);
    alg::graph graph(inputFile);

    auto sccSizes = graph.findSccSizes();
    std::sort(sccSizes.begin(), sccSizes.end(), std::greater<unsigned>());
    printSizes(sccSizes);

    return 0;
}
