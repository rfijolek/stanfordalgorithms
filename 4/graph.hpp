#ifndef ALG_GRAPH_HPP
#define ALG_GRAPH_HPP

#include <vector>
#include <unordered_map>
#include <forward_list>
#include <fstream> 

namespace alg
{

class graph
{
private:    
    struct node
    {
        node(unsigned label): label_(label), explored_(false) {}

        unsigned label_;
        bool explored_;
        std::forward_list<node*> neighbours_;
    };

private:
    using graphStructure = std::unordered_map<unsigned, node*>;
    using nodeNeighbours = std::forward_list<node*>;
    using orderedSources = std::forward_list<unsigned>;  // ordered by finishing times

public:
    graph() = default;

    graph(std::ifstream& file)
    {
        unsigned head, tail;

        while(file.good())
        {
            file >> head;
            file >> tail;

            addNodeIfNew(head);
            addNodeIfNew(tail);
            addEdge(head, tail);
        }
    }

    graph(const graph&) = delete;
    graph& operator=(const graph&) = delete;
    graph(graph&&) = default;
    graph& operator=(graph&&) = default;

    ~graph()
    {
        for (const auto& node: graph_)
            delete node.second;
    }

    std::vector<unsigned> findSccSizes()
    {
        auto sourcesOrderedByFinishingTime = computeDfsFinishingTimesOnReversedGraph();
        return computeSccSizes(sourcesOrderedByFinishingTime);
    }

private:
    void addNodeIfNew(unsigned label)
    {
        if (graph_.find(label) == graph_.end())
        {
            node* n = new node(label);
            graph_.insert(graphStructure::value_type(label, n));
        }
    }

    void addEdge(unsigned head, unsigned tail)
    {
        const auto headNode = graph_.at(head);
        const auto tailNode = graph_.at(tail);
        headNode->neighbours_.push_front(tailNode);
    }

    void mark(unsigned label)
    {
        const auto n = graph_.at(label);
        n->explored_ = true;
    }

    nodeNeighbours& getNodeNeighbours(unsigned label)
    {
        const auto n = graph_.at(label);
        return n->neighbours_;
    }

    void clearMarkers()
    {
        for (const auto& node: graph_)
            node.second->explored_ = false;
    }

    graph cloneReversed()
    {
        graph rev;

        for (const auto& node: graph_)
        {
            rev.addNodeIfNew(node.first);
            rev.addReversedEdges(node.second);
        }
        return rev;
    }

    void addReversedEdges(const node* n)
    {
        for (auto neighbour: n->neighbours_)
        {
            addNodeIfNew(neighbour->label_);
            addEdge(neighbour->label_, n->label_);
        }
    }

    orderedSources computeDfsFinishingTimesOnReversedGraph()
    {
        orderedSources sources;
        auto gRev = cloneReversed();

        for (const auto& node: gRev.graph_)
        {
            if (!node.second->explored_) gRev.dfs(node.first, sources);
        }
        return sources;
    }

    void dfs(unsigned source, orderedSources& sources)
    {
        mark(source);

        for (auto neighbour: getNodeNeighbours(source))
        {
            if (!neighbour->explored_) dfs(neighbour->label_, sources);
        }
        sources.push_front(source);
    }

    std::vector<unsigned> computeSccSizes(const orderedSources& sources)
    {
        std::vector<unsigned> sccSizes;
        unsigned currentSccSize;

        for (const auto source: sources)
        {
            currentSccSize = 0;
            auto node = graph_[source];
            if (!node->explored_) dfs(node->label_, currentSccSize);
            sccSizes.push_back(currentSccSize);
        }
        return sccSizes;
    }

    void dfs(unsigned source, unsigned& sccSize)
    {
        mark(source);
        sccSize++;

        for (auto neighbour: getNodeNeighbours(source))
        {
            if (!neighbour->explored_) dfs(neighbour->label_, sccSize);
        }
    }

private:
    graphStructure graph_;
};

}  // namespace alg

#endif  // ALG_GRAPH_HPP
