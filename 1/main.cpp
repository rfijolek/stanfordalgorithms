#include <iostream>
#include <fstream> 
#include <vector>
#include <iterator>
#include "InversionsCounter.hpp"

std::vector<unsigned> readInputFromFile(std::ifstream& inputFile)
{
    return std::vector<unsigned>(
            std::istream_iterator<unsigned>(inputFile),
            std::istream_iterator<unsigned>());
}

int main(int argc, char** argv)
{
    std::ifstream inputFile(argv[1]);
    auto inversions = InversionsCounter().count(readInputFromFile(inputFile));

    std::cout << "Number of inversions: " << inversions << std::endl;

    return 0;
}
