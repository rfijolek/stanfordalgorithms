#include "InversionsCounter.hpp"
#include <iostream>
#include <algorithm>

long long InversionsCounter::count(Numbers numbers)
{
    return countAndSort(numbers, numbers.size());
}

long long InversionsCounter::countAndSort(Numbers& numbers, unsigned n)
{
    if (n == 1) return 0;

    auto firstHalf = Numbers(numbers.begin(), numbers.begin() + n/2);
    auto firstHalfInv = countAndSort(firstHalf, n/2);

    auto secondHalf = Numbers(numbers.begin() + n/2, numbers.end());
    auto secondHalfInv = countAndSort(secondHalf, n - n/2);

    auto splitInv = countSplitInvAndSort(numbers, firstHalf, secondHalf);
    return firstHalfInv + secondHalfInv + splitInv;
}

long long InversionsCounter::countSplitInvAndSort(
    Numbers& whole, const Numbers& first, const Numbers& second)
{
    unsigned inversions = 0;
    auto first1 = first.begin(), last1 = first.end();
    auto first2 = second.begin(), last2 = second.end();
    auto result = whole.begin();

    while (true)
    {
        if (first1 == last1)
        {
            std::copy(first2, last2, result);
            break;
        }

        if (first2 == last2)
        {
            std::copy(first1, last1, result);
            break;
        }

        if (*first1 < *first2) *result++ = *first1++;
        else
        {
            *result++ = *first2++;
            inversions += std::distance(first1, last1);
        }
    }
    return inversions;
}
