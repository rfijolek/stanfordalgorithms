#include <vector>

class InversionsCounter
{
private:
    using Numbers = std::vector<unsigned>;

public:
    long long count(Numbers numbers);

private:
    long long countAndSort(Numbers& numbers, unsigned n);

    long long countSplitInvAndSort(Numbers& whole, const Numbers& first, const Numbers& second);
};
