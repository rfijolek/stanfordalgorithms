#ifndef ALG_GRAPH_HPP
#define ALG_GRAPH_HPP

#include <vector>
#include <unordered_map>
#include <forward_list>
#include <fstream> 
#include <algorithm>
#include <functional>
#include <sstream>
#include <utility>

namespace alg
{

class graph
{
private:
    struct edge;

    struct node
    {
        node(unsigned label): label_(label), indexInHeap_(-1) {}

        unsigned label_;
        int indexInHeap_;
        std::forward_list<edge> outgoingEdges_;
    };

    struct edge
    {
        edge(node* head, unsigned weight): head_(head), weight_(weight) {}

        node* head_;
        unsigned weight_;
    };

    struct dijkstraHeapNode
    {
        dijkstraHeapNode(node* data, node* minTail, unsigned distance):
            data_(data), minTail_(minTail), minDistFromFinished_(distance) {}

        bool operator>(const dijkstraHeapNode& other) const
        {
            return minDistFromFinished_ > other.minDistFromFinished_;
        }

        bool operator<(const dijkstraHeapNode& other) const
        {
            return minDistFromFinished_ < other.minDistFromFinished_;
        }

        friend void swap(dijkstraHeapNode& lhs, dijkstraHeapNode& rhs)
        {
            using std::swap;

            swap(lhs.data_, rhs.data_);
            swap(lhs.minTail_, rhs.minTail_);
            swap(lhs.minDistFromFinished_, rhs.minDistFromFinished_);
            swap(lhs.data_->indexInHeap_, rhs.data_->indexInHeap_);
        }

        dijkstraHeapNode& operator=(dijkstraHeapNode other)
        {
            swap(*this, other);
            return *this;
        }

        node* data_;
        node* minTail_;
        unsigned minDistFromFinished_;  // heap key
    };


private:
    using graphStructure = std::unordered_map<unsigned, node*>;
    using distancesToNodes = std::unordered_map<unsigned, unsigned>;
    using dijkstraHeap = std::vector<dijkstraHeapNode>;
    using minHeapCmp = std::greater<dijkstraHeapNode>;
    using pathLengths = std::unordered_map<unsigned, unsigned>;

public:
    graph() = default;

    graph(std::ifstream& file)
    {
        std::string line;
        unsigned tail, head, weight;

        while (std::getline(file, line))
        {
            std::istringstream iss(line);
        
            iss >> tail;
            addNodeIfNew(tail);

            while(iss)
            {
                iss >> head;
                iss.ignore(1, ',');
                iss >> weight;

                addNodeIfNew(head);
                addEdge(tail, head, weight);
            }
        }
    }

    graph(const graph&) = delete;
    graph& operator=(const graph&) = delete;
    graph(graph&&) = default;
    graph& operator=(graph&&) = default;

    ~graph()
    {
        for (const auto& node: graph_)
            delete node.second;
    }

    pathLengths findShortestPathsFrom(unsigned source)
    {
        auto sourceNeighbours = getDistancesToNeighbours(source);
        auto nodesToBeProcessed = prepareHeapOfNodesToBeProcessed(source, sourceNeighbours);
        updateGraphNodesWithIndicesInHeap(nodesToBeProcessed);

        return findShortestPathsToNodes(source, std::move(nodesToBeProcessed));
    }

private:
    void addNodeIfNew(unsigned label)
    {
        if (graph_.find(label) == graph_.end())
        {
            node* n = new node(label);
            graph_.insert(graphStructure::value_type(label, n));
        }
    }

    void addEdge(unsigned tail, unsigned head, unsigned weight)
    {
        const auto tailNode = graph_.at(tail);
        const auto headNode = graph_.at(head);
        tailNode->outgoingEdges_.emplace_front(headNode, weight);
    }

    distancesToNodes getDistancesToNeighbours(unsigned source)
    {
        distancesToNodes distances;
        
        for (const auto& edgeToNeighbour: graph_[source]->outgoingEdges_)
            distances[edgeToNeighbour.head_->label_] = edgeToNeighbour.weight_;

        return distances; 
    }

    dijkstraHeap prepareHeapOfNodesToBeProcessed(
        unsigned source, const distancesToNodes& sourceNeighbours)
    {
        using namespace std::placeholders; 
        dijkstraHeap toBeProcessed;

        std::transform(graph_.begin(), graph_.end(), std::back_inserter(toBeProcessed), std::bind(
            &graph::makeHeapNodeFromGraphNode, this, source, _1, std::cref(sourceNeighbours)));
        toBeProcessed.erase(std::remove_if(toBeProcessed.begin(), toBeProcessed.end(),
             [source] (const dijkstraHeapNode& node) { return node.data_->label_ == source; }));
        std::make_heap(toBeProcessed.begin(), toBeProcessed.end(), minHeapCmp());

        return toBeProcessed;
    }

    dijkstraHeapNode makeHeapNodeFromGraphNode(unsigned source,
        const graphStructure::value_type& node, const distancesToNodes& sourceNeighbours)
    {
        const unsigned unreachableWeight = 1000000;
        auto foundEdge = sourceNeighbours.find(node.first);

        if (foundEdge == sourceNeighbours.end())
            return dijkstraHeapNode(node.second, graph_[source], unreachableWeight);
        return dijkstraHeapNode(node.second, graph_[source], foundEdge->second);
    }

    void updateGraphNodesWithIndicesInHeap(const dijkstraHeap& heap)
    {
        for (unsigned i = 0; i < heap.size(); ++i)
            heap[i].data_->indexInHeap_ = i;
    }

    pathLengths findShortestPathsToNodes(unsigned source, dijkstraHeap toBeProcessed)
    {
        pathLengths lengths;
        lengths[source] = 0;

        while (!toBeProcessed.empty())
        {
            auto min = *toBeProcessed.begin();
            lengths[min.data_->label_] = lengths[min.minTail_->label_] + min.minDistFromFinished_;

            pop_heap(toBeProcessed);
            maintainHeapInvariants(toBeProcessed, lengths, min);
        }
        return lengths;
    }
    
    void maintainHeapInvariants(dijkstraHeap& toBeProcessed,
        const pathLengths& lengths, const dijkstraHeapNode& min)
    {
        for (const auto& edgeToNeighbour: min.data_->outgoingEdges_)
        {
            if (isNodeInHeap(edgeToNeighbour.head_))
            {
                auto removedHead = remove_heap(toBeProcessed, edgeToNeighbour.head_);
                removedHead.minDistFromFinished_ = std::min(removedHead.minDistFromFinished_,
                    lengths.at(min.data_->label_) + edgeToNeighbour.weight_);

                auto newHeapIndex = push_heap(toBeProcessed, removedHead);
                edgeToNeighbour.head_->indexInHeap_ = newHeapIndex;
            }
        }
    }

    bool isNodeInHeap(node* n)
    {
        return n->indexInHeap_ != -1;
    }

    void markNodeAsNotInHeapAnymore(node* n)
    {
        n->indexInHeap_ = -1;
    }

    dijkstraHeapNode remove_heap(dijkstraHeap& heap, node* n)
    {
        auto i = static_cast<unsigned>(n->indexInHeap_);
        const auto removedElement = heap[i];

        swap(heap[i], heap.back());
        heap.pop_back();

        while (heap[i] < heap[i/2])
        {
            swap(heap[i], heap[i/2]);
            i /= 2;        
        }

        if (i == static_cast<unsigned>(n->indexInHeap_))
        {
            min_heapify(heap, i+1);
        }

        return std::move(removedElement);
    }

    unsigned push_heap(dijkstraHeap& heap, const dijkstraHeapNode& n)
    {
        heap.push_back(n);
        auto i = heap.size() - 1;

        while (heap[i] < heap[i/2])
        {
            swap(heap[i], heap[i/2]);
            i /= 2;        
        }
        return i;
    }

    void pop_heap(dijkstraHeap& heap)
    {
        swap(heap.front(), heap.back());
        markNodeAsNotInHeapAnymore(heap.back().data_);
        heap.pop_back();
        min_heapify(heap, 1);
    }

    void min_heapify(dijkstraHeap& heap, unsigned i)
    {
        unsigned left = 2*i;
        unsigned right = 2*i + 1;
        unsigned min = i;

        if (left <= heap.size() && heap[left-1] < heap[i-1]) min = left;
        if (right <= heap.size() && heap[right-1] < heap[min-1]) min = right;
        
        if (min != i)
        {
            swap(heap[i-1], heap[min-1]);
            min_heapify(heap, min);
        }
    }

private:
    graphStructure graph_;
};

}  // namespace alg

#endif  // ALG_GRAPH_HPP
