#include <fstream> 
#include <iostream>
#include "graph.hpp"

int main(int argc, char** argv)
{
    std::ifstream inputFile(argv[1]);
    alg::graph graph(inputFile);
    auto shortestPaths = graph.findShortestPathsFrom(1);

    std::cout << shortestPaths[7] << ",";
    std::cout << shortestPaths[37] << ",";
    std::cout << shortestPaths[59] << ",";
    std::cout << shortestPaths[82] << ",";
    std::cout << shortestPaths[99] << ",";
    std::cout << shortestPaths[115] << ",";
    std::cout << shortestPaths[133] << ",";
    std::cout << shortestPaths[165] << ",";
    std::cout << shortestPaths[188] << ",";
    std::cout << shortestPaths[197];

    return 0;
}
